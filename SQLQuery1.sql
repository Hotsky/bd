use Zavod

create table Izdelye(
idIzdelye int IDENTITY(1,1)not null primary key,
NameIzdelya nvarchar(50),
KodIzdelya int,
ObzaGod int,
Izmeren nvarchar(5)
)

create table Material(
idMaterial int IDENTITY(1,1)not null primary key,
NameMaterial nvarchar(50),
TypeMaterial nvarchar(50),
CenaOfOne money,
EdinicaIzmer nvarchar(10),
Otmetka nvarchar(50)
)


create table SpecifOfMaterial(
idSpecifOfMaterial int IDENTITY(1,1)not null primary key,
Kolichestvo int,
DateSetup date,
DateCansel date,
idMaterial int not null REFERENCES Material(idMaterial),
idIzdelye int not null REFERENCES Izdelye(idIzdelye)
)

Insert into Izdelye( [NameIzdelya], [KodIzdelya], [ObzaGod], [Izmeren])
values ('������', '0231', '200', '���.'),
		('�����', '5497', '60', '��.'),
		('������', '9753', '150', '��.'),
		('�����', '3165', '200', '��.'),
		('������� ������', '6487', '15', '��.')
select *from Izdelye;

insert into Material([NameMaterial], [TypeMaterial], [CenaOfOne], [EdinicaIzmer], [Otmetka])
values ('�����', '��������', '20', '�����', '�������'),
		('������', '����� �������', '10', '��.', '������'),
		('������', '������', '20', '�', '�������'),
		('�����', '������� �����', '30', '��.', '������'),
		('������', '������� �����', '700', '��.', '�������')
select *from Material;


insert into SpecifOfMaterial([Kolichestvo],[DateSetup],[DateCansel],[idMaterial],[idIzdelye])
values ('50', '19.07.1995', '14.07.2010', '1','1'),
		('12', '23.04.1980', '28.03.2010', '2', '2'),
		('100', '08.10.2001', '28.03.2019', '3', '3'),
		('100', '19.10.2015', '28.03.2050', '4', '4'),
		('10', '11.10.1980', '28.03.1998', '5', '5')
select *from SpecifOfMaterial;



 SELECt Izdelye.NameIzdelya, Material.TypeMaterial from
 Izdelye join Material on Material.idMaterial = Izdelye.idIzdelye
 WHERE TypeMaterial = '������� �����';
 

 select  Izdelye.NameIzdelya, SpecifOfMaterial.DateSetup, SpecifOfMaterial.DateCansel from
 Izdelye join SpecifOfMaterial on SpecifOfMaterial.idSpecifOfMaterial = Izdelye.idIzdelye
 where DateSetup < '2000.01.01' and DateCansel > '2000.12.31';

 select Material.NameMaterial, Material.CenaOfOne, SpecifOfMaterial.DateSetup, SpecifOfMaterial.DateCansel 
 from Material join SpecifOfMaterial on SpecifOfMaterial.idSpecifOfMaterial = Material.idMaterial
 where CenaOfOne < '15' and DateSetup < '2000.01.01' and DateCansel > '2000.12.31';

select Material.NameMaterial, Material.CenaOfOne, SpecifOfMaterial.DateSetup, SpecifOfMaterial.DateCansel
from Material join SpecifOfMaterial on SpecifOfMaterial.idSpecifOfMaterial = Material.idMaterial
where CenaOfOne < '21' and CenaOfOne > '19' and DateSetup < '2000.01.01' and DateCansel > '2000.12.31';